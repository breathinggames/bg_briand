# Les aventures du Briand

A game for health co-created by commonners of [Breathing Games](http://www.breathinggames.net), and released under free/libre licence.

Developed on [Unity 3D](http://unity3d.com), to be used on a [Firefox](https://www.mozilla.org/en-US/firefox/products/) browser or [Android](https://www.android.com) phone, with a [breathing device](https://gitlab.com/breathinggames/bg/wikis/5-hardware).

**Read the [documentation](https://drive.google.com/drive/u/0/folders/0B6qPnEPJv0uxa0tCN0VyNWR2ekk).**


## In short
Breath to advance your ship and solve the challenges the crew faces.


## Contributors
Contributors are welcome. To join, contact info (at) breathinggames.net

- John Danger
- Damien Fangous
- Cem Koker
- John Willimann
- Antoine Chevalier 
- Kostia Miteskyy
- Stéphane Geiser