﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FaderScript : MonoBehaviour {

	Animator _animator;
	public string nextScene;

	void Update() {
		if (Input.GetKeyDown(KeyCode.A)) {
			fadeIn();
		}
		if (Input.GetKeyDown(KeyCode.S)) {
			fadeOut();
		}
	}


	void Start () {
		_animator = gameObject.GetComponent<Animator>();
	}

	public void fadeIn () {
		_animator.SetInteger("Fader", 1);
	}


	public void fadeOut () {
		_animator.SetInteger("Fader", -1);
	}

	public void LoadNextScene() {
		SceneManager.LoadScene(nextScene);
	}

	public void LoadScene(string scene) {
		SceneManager.LoadScene(scene);
	}

}
