﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class timeout : MonoBehaviour {

	public int secondsToWait;
	private WaitForSeconds m_StartWait;         
	public GameObject Fader;
	private float timeRemaining;

	void Start() {
		timeRemaining = secondsToWait;
		Debug.Log ("begin");


	}

	void Update() {
		timeRemaining -= Time.deltaTime;
		float seconds = Mathf.Floor(timeRemaining % 60);
//		Debug.Log (seconds);
		if (seconds == 0) {
			Fader.GetComponent<FaderScript>().fadeOut();
		}
	}
		
}
