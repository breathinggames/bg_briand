﻿using UnityEngine;
using System.Collections;


public class InputController : MonoBehaviour {


	public GameObject IceBergManager;
	public GameObject Bateau;
	OnlyExpirationInputController k;
	Exercice e;

	// Use this for initialization
	void Start () {
		k = new KeyboardInputController(3f);
		e=new ExerciceTest(k);
		k.SetExercice(e);

	}
	
	// Update is called once per frame
	void Update () {	
		k.Update();	
		if (k.GetInputState() == BreathingState.EXPIRATION) {
			// Debug.Log (k.GetStrength());
			IceBergManager.GetComponent<SC_IcebergManager>().playerSpeed = 100;
			Bateau.GetComponent<boatAnimationController>().Wind = 1;
		}
		else {
			IceBergManager.GetComponent<SC_IcebergManager>().playerSpeed = 0;
			Bateau.GetComponent<boatAnimationController>().Wind = 0;
			// Debug.Log (0);
		}
	}
}
