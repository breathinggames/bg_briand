﻿using UnityEngine;
using System.Collections;

public class IcebergController : MonoBehaviour {

	public float moveSpeed = 30f;

	void Update () {
		transform.Translate(-Vector3.left * moveSpeed * Time.deltaTime);
	}
}
