﻿using UnityEngine;
using System.Collections;

public class BoatController : MonoBehaviour {

	public float moveSpeed = 20f;
	public float turnSpeed = 10f;

	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.UpArrow))
			transform.Translate(Vector3.up * moveSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.LeftArrow))
			transform.Rotate(Vector3.down, -turnSpeed * Time.deltaTime);

		if(Input.GetKey(KeyCode.RightArrow))
			transform.Rotate(Vector3.right, turnSpeed * Time.deltaTime);
	}
}
