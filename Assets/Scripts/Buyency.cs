﻿using UnityEngine;
using System.Collections;

public class Buyency : MonoBehaviour {

	public float amplitude = 30f;

	void Update() {
		transform.rotation = Quaternion.Euler(  Mathf.Cos(Time.realtimeSinceStartup) * amplitude,transform.rotation.eulerAngles.y, Mathf.Sin(Time.realtimeSinceStartup) * amplitude); 
	}
}
