using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SC_IcebergManager : MonoBehaviour {

	public GameObject[] a_Ateliers;
	public int i_InstanceAtelier = 3;
	public List<GameObject> a_PoolAtelier;
	public List<GameObject> a_AteliersActifs;
	public float f_OffsetToDeactivate;
	private float f_DepthNextAtelier;
	public int i_NbAteliersActifs;
	private GameObject _Player;
	private bool b_ArrayFilled;
	// private GameObject[] a_DestructiblesTemp;
	public float icebergSpeed;
	public float playerSpeed;
	// Espce horizontal dans lequel on spawn les objets
	public float espaceHorizon = 1000f;


	public Transform spawnPoint;

	// Use this for initialization
	void Start () 
	{
		//L'atelier 0 sera toujours l'atelier de départ
		f_DepthNextAtelier = 0;

		b_ArrayFilled = false;

		//Je remplis la pool d'atelier
		int l = 0;
		int secure = 0;

		while(l < a_Ateliers.Length && secure < 1000)
		{
			for(int i = 0; i < i_InstanceAtelier; i ++)
			{
				if(l == 0)
				{
					i += i_InstanceAtelier-1;
				}

				GameObject clone = Instantiate(a_Ateliers[l], new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z + espaceHorizon - Random.Range(0, espaceHorizon)), Quaternion.identity) as GameObject;
				a_PoolAtelier.Add(clone);
			}

			l++;
			secure ++;
		}

		_Player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(!b_ArrayFilled)
		{
			AddAtelier(0);
			b_ArrayFilled = true;
		}

		AteliersHandeler ();
		MoveIceberg (icebergSpeed, playerSpeed);
	}

	private void AteliersHandeler ()
	{
		//1. je regarde si l'atelier 1.y + sa longueur + f_OffsetToDeactivate est > que player.y ----> 
		//// je le désactive et je le retire des ateliers actifs et je l'add à la pool d'atelier

		//2. Si le compte d'atelier dans les ateliers actifs est < i_NbAteliersActifs j'add un nouvel atelier
		////je retire un atelier random de la PoolAtelier et je l'ajoute aux ateliers actifs
		/// le random ne doit pas prendre l'atelier 0
//		if((a_AteliersActifs[0].transform.position.x - a_AteliersActifs[0].GetComponent<SC_IceBerg>().f_Length - f_OffsetToDeactivate) > _Player.transform.position.x)

		if(a_AteliersActifs[0].transform.position.x  - 200f > _Player.transform.position.x)
		{
			RemoveAtelier(0);
		}

		if(a_AteliersActifs.Count < i_NbAteliersActifs)
		{
			AddAtelier(Random.Range(1, a_PoolAtelier.Count));
		}
	}

	private void AddAtelier (int i_IndexPoolAtelier)
	{
		//je le place au bon endroit (les ateliers sont placé par rapport à leur angle superieur gauche)
		//je l'active
		//ajoute sa longueure à la profondeur pour le prochain atelier
		//ajoute un atelier dans le tableau des ateliers actifs
		//je le retire de la pool d'atelier

		int rand = Random.Range(0, 1);
		if (rand == 0) {
			a_PoolAtelier[i_IndexPoolAtelier].transform.position = new Vector3(spawnPoint.transform.position.x, transform.position.y, espaceHorizon + Random.Range(0, espaceHorizon));
		} else {
			a_PoolAtelier[i_IndexPoolAtelier].transform.position = new Vector3(spawnPoint.transform.position.x, transform.position.y, espaceHorizon - Random.Range(0, espaceHorizon));
		}

		f_DepthNextAtelier -= a_PoolAtelier[i_IndexPoolAtelier].GetComponent<SC_IceBerg>().f_Length;

		a_AteliersActifs.Add(a_PoolAtelier[i_IndexPoolAtelier]);
		a_PoolAtelier.RemoveAt(i_IndexPoolAtelier);
	}

	private void RemoveAtelier (int i_IndexAtelierActif)
	{
		a_PoolAtelier.Add(a_AteliersActifs[i_IndexAtelierActif]);
		a_AteliersActifs.RemoveAt(i_IndexAtelierActif);
	}

	private void MoveIceberg (float speed_iceberg, float playerSpeed) {
		for(int i = 0; i < a_AteliersActifs.Count; i ++)
		{
			float posY = a_AteliersActifs[i].transform.position.x;
			a_AteliersActifs[i].transform.position = new Vector3 (a_AteliersActifs[i].transform.position.x + (speed_iceberg + playerSpeed) * 2f + Time.deltaTime, a_AteliersActifs[i].transform.position.y, a_AteliersActifs[i].transform.position.z);
			// a_AteliersActifs[i].transform.Translate(Vector3.right * (speed_iceberg + playerSpeed));
		}
	}
}