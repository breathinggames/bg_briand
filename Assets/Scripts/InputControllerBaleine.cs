﻿using UnityEngine;
using System.Collections;


public class InputControllerBaleine : MonoBehaviour {
	
	public GameObject IceBergManager;
	public GameObject Bateau;
	Animator _anim;
	public int nombreSauts;
	int compteur;
	void Start () {
		
		_anim = Bateau.GetComponent<Animator>();
	}
	

	void Update () {	
		if (Input.GetKeyDown(KeyCode.Space)) {
			_anim.SetBool("Jump", true);
		
		}	
		if (Input.GetKeyUp(KeyCode.Space)) {
			compteur++;
			_anim.SetBool("Jump", false);
		}	
		if (compteur == 2) {
			_anim.SetBool("Descends", true);
		}
	}
}
