using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
	LE SINGE EXPLIQUE COMMENT DESCENDRE DU DOS DE LA BALEINE
*/

public class SC_Sequence4 : MonoBehaviour {

	public AudioSource as_Speaker;
	public List<AudioClip> a_Dialogue;
	public GameObject go_Singe;

	// Use this for initialization
	void Start () 
	{
		as_Speaker = this.GetComponent<AudioSource>();
		go_Singe.SetActive (false);

		StartCoroutine (Sequence());
	}


	// Update is called once per frame
	void Update () 
	{

	}

	IEnumerator Sequence () {

		go_Singe.SetActive (true);
		as_Speaker.clip = a_Dialogue[0];
		as_Speaker.Play();

		yield return new WaitForSeconds (a_Dialogue[0].length);

		go_Singe.SetActive (false);
		as_Speaker.Stop();

		yield break;
	}
	/*
	IEnumerator Sequence () {

		go_Singe.SetActive (true);
		as_Speaker.clip = a_Dialogue[0];
		as_Speaker.Play();

		yield return new WaitForSeconds (a_Dialogue[0].length);

		go_Singe.SetActive (false);
		as_Speaker.Stop();

		yield return new WaitForEndOfFrame ();

		go_Capitaine.SetActive (true);
		as_Speaker.clip = a_Dialogue[1];
		as_Speaker.Play();

		yield return new WaitForSeconds (a_Dialogue[1].length);

		go_Capitaine.SetActive (false);
		as_Speaker.Stop();

		yield break;
	}*/
}