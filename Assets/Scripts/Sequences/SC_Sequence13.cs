using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
SEQUENCE 
 */

public class SC_Sequence13 : MonoBehaviour {

	public AudioSource as_Speaker;
	public List<AudioClip> a_Dialogue;
	public GameObject go_Capitaine;
	public GameObject go_Johl;

	// Use this for initialization
	void Start () 
	{
		as_Speaker = this.GetComponent<AudioSource>();
		go_Capitaine.SetActive (false);
		go_Johl.SetActive (false);

		StartCoroutine (Sequence());
	}


	// Update is called once per frame
	void Update () 
	{

	}

	IEnumerator Sequence () {

		go_Johl.SetActive (true);
		as_Speaker.clip = a_Dialogue[0];
		as_Speaker.Play();

		yield return new WaitForSeconds (a_Dialogue[0].length);

		go_Johl.SetActive (false);
		as_Speaker.Stop();

		yield return new WaitForEndOfFrame ();

		go_Capitaine.SetActive (true);
		as_Speaker.clip = a_Dialogue[1];
		as_Speaker.Play();

		yield return new WaitForSeconds (a_Dialogue[1].length);

		go_Capitaine.SetActive (false);
		as_Speaker.Stop();

		yield break;
	}

}