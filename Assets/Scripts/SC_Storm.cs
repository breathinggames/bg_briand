﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SC_Storm : MonoBehaviour {

	public int i_NbCloud;
	public GameObject go_Cloud;
	public List<GameObject> a_Clouds;
	public List<Vector3> a_CloudsPos;
	public AnimationCurve ac_CloudsFleeingSpeed;
	private float f_breathIntensity;
	public float f_DistanceOuttaScreen;

	// Use this for initialization
	void Start () {
		f_breathIntensity = 0;

		//Instantiation des nuages aléatoirement sur l'écran
		int l = 0;
		int secure = 0;

		while(l < i_NbCloud && secure < 1000)
		{
//			float posX = Random.Range(-Screen.width*0.5f, Screen.width*0.5f);
//			float posY = Random.Range(-Screen.height*0.5f, Screen.height*0.5f);
			float posX = Random.Range(-9, 9);
			float posY = Random.Range(-5, 5);
			GameObject clone = Instantiate(go_Cloud, new Vector3(0, 0, 0f), this.transform.rotation) as GameObject;
			clone.transform.parent = this.transform;
//			clone.transform.position = new Vector3(this.transform.position.x + posX,
//			                                       this.transform.position.y + posY, 
//			                                       0);
			clone.transform.localPosition = new Vector3(posX,
			                                       posY, 
			                                       0);
			a_CloudsPos.Add (clone.transform.position);
			a_Clouds.Add(clone);
			
			l++;
			secure++;
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Systeme temporaire pour simuler le souffle du joueur
		if (Input.GetKey (KeyCode.Space)) {
			if(f_breathIntensity < 10) {
				f_breathIntensity +=0.2f;
				CloudFleeing(f_breathIntensity);
			}
		} else {
			if(f_breathIntensity > 0){
				f_breathIntensity -=0.2f;
				CloudFleeing(f_breathIntensity);
			}
		}

		//Evenement qui termine l'orage
		if (f_breathIntensity > 10) {
			Destroy(this.gameObject);
		}
	}

	private void CloudFleeing (float intensity) {
		for (int i = 0; i < a_Clouds.Count; i++) {
//			a_Clouds [i].transform.position = Vector3.Lerp (a_CloudsPos [i], 
//		                                                GetFinalPos (a_CloudsPos [i]), 
//		            									ac_CloudsFleeingSpeed.Evaluate (f_breathIntensity * 0.1f));


			a_Clouds[i].GetComponent<SpriteRenderer>().color = new Color(a_Clouds[i].GetComponent<SpriteRenderer>().color.r,
			                                                             a_Clouds[i].GetComponent<SpriteRenderer>().color.g,
			                                                             a_Clouds[i].GetComponent<SpriteRenderer>().color.b,
			                                                             1 - ac_CloudsFleeingSpeed.Evaluate (f_breathIntensity * 0.1f));
		}
	}


	private Vector3 GetFinalPos (Vector3 center) {
		Vector3 dir = center - this.transform.parent.position;
		float f_Ang = Vector3.Angle (Vector3.forward, dir);
		float posZ;
		float posY;

		posZ = Mathf.Cos (Mathf.Deg2Rad * f_Ang) * f_DistanceOuttaScreen;
		posY = Mathf.Sin (Mathf.Deg2Rad * f_Ang) * f_DistanceOuttaScreen;

		return new Vector3 (center.x, posY, posZ);
	}
}
