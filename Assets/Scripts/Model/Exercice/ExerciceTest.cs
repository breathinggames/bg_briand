using System;
using System.Collections.Generic;

/// <summary>
/// This class implements a decreasing "Drainage autogène" which consists of 3 sequence of breathings
/// which are made at 3 different volumes. The "Decreasing" is because we start at the highest volume.
/// </summary>
public class ExerciceTest:Exercice
{

	public ExerciceTest ( InputController_I inputController)
	{

		//Transition from low to high
		breathings = new List<Breathing> (1);
		breathings.Add(new Breathing(1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 10.0f,BreathingState.HOLDING_BREATH, inputController));


		indexActualBreathing = 0;
	}
}


