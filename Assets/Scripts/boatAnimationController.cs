﻿using UnityEngine;
using System.Collections;

public class boatAnimationController : MonoBehaviour {

	public int Wind = 0;
	private Animator _animator;

	void Start () {
		 _animator = gameObject.GetComponent<Animator>();
	}
	
	void Update () {
		_animator.SetInteger("Wind", Wind);
	}
}
